async function requestRepos() {
    var accesToken = document.getElementById("password").value;
    var errorMessage = document.getElementById("message");
    var repoList = document.getElementById("repos-list");

    const responce = await fetch("https://api.github.com/user/repos", {
        headers: {
          Authorization: "Bearer " + accesToken
        }
    });
    const repoData = await responce.json();
    console.log(repoData);

    if (repoData.message) {
        console.log("nix gut");
        errorMessage.innerHTML = "Authentication failed";
        repoList.innerHTML = "";
    } else {
        errorMessage.innerHTML = "";
        repoData.forEach(repo => {
            li = document.createElement("li");
            text = document.createTextNode(repo.name);
            li.appendChild(text);
            repoList.appendChild(li);
        });
    }
}