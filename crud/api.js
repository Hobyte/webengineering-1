function everything() {
    fetch('https://dummyjson.com/products')
    .then(res => res.json())
    .then(console.log);
}

function one() {
    var id = document.getElementById('single').value
    fetch(`https://dummyjson.com/products/${id}`)
    .then(res => res.json())
    .then(console.log);
}

function search() {
    var term = document.getElementById('product').value
    fetch(`https://dummyjson.com/products/search?q=${term}`)
    .then(res => res.json())
    .then(console.log);
}

function add() {
    fetch('https://dummyjson.com/products/add', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
        title: 'BMW Pencil',
    })
    })
    .then(res => res.json())
    .then(console.log);
}

function update() {
    fetch('https://dummyjson.com/products/1', {
    method: 'PUT', /* or PATCH */
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
        title: 'iPhone Galaxy +1'
    })
    })
    .then(res => res.json())
    .then(console.log);
}

function remove() {
    fetch('https://dummyjson.com/products/1', {
    method: 'DELETE',
    })
    .then(res => res.json())
    .then(console.log);
}
