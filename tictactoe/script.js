let board = ['', '', '', '', '', '', '', '', ''];
let currentPlayer = 'X';
let gameOver = false;

function makeMove(index) {
    if (!gameOver) {
        let cell = document.querySelector(`#board #cell-${index.toString()}`);
        if (board[index] === '') {
            cell.innerText = currentPlayer;
            board[index] = currentPlayer;
            checkGameOver();
            changeCurrentPlayer();
        }
    }
    
}

function changeCurrentPlayer() {
 if (currentPlayer == 'X') {
    currentPlayer = 'O'
 } else {
    currentPlayer = 'X'
 }
}

function checkGameOver() {
    const winCombinations = [
        [0, 1, 2], [3, 4, 5], [6, 7, 8],
        [0, 3, 6], [1, 4, 7], [2, 5, 8],
        [0, 4, 8], [2, 4, 6]
    ];

    for (let combination of winCombinations) {
        let [a, b, c] = combination;
        if (board[a] === currentPlayer & board[b] === currentPlayer & board[c] === currentPlayer) {
            gameOver = true;
            document.getElementById("instruction").innerText = `Player ${currentPlayer} Wins`;
        }
    }
}

function resetGame() {
    resetBoard();
    resetVariables();

    gameOver = false;
}

function resetBoard() {
    board = ['X', '', '', '', '', '', '', '', ''];
    let htmlBoard = document.getElementById("board");
    htmlBoard.childNodes.forEach(element => {
        element.innerText = "";
    });

    document.getElementById("instruction").innerText = '';
}

function resetVariables() {
    board = ['', '', '', '', '', '', '', '', ''];
    currentPlayer = 'X';
    gameOver = false;
}
