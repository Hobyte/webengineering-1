let currentResult = '';

function appendNumber(number) {
    currentResult += number;
    updateDisplay();
}

function appendOperator(operator) {
    currentResult += operator;
    updateDisplay();
}

function calculate() {
    currentResult = eval(currentResult);
    updateDisplay();
}

function clearResult() {
    currentResult = '';
    updateDisplay();
}

function updateDisplay() {
    document.getElementById("result").value = currentResult;
}